let tasks = []


function createTask(name, description, estDays) {
  if (!name || !description || !estDays) {
    handleError(name, 'empty');
    return;
  }

  const task = tasks.filter(task => task.name === name)[0];

  if (task) {
    handleError(name, 'name');
    return;
  }

  if (estDays <= 0) {
    handleError(estDays, 'estDays');
    return;
  }

  tasks.push({
    "name" : name,
    "description" : description,
    "estDays" : parseInt(estDays),
    "inproject" : ''
  })
  console.log(`[Task] ${name} added! It takes ${estDays} estimated ${ estDays > 1 ? 'days' : 'day' } to finish.`);    
}


function assignTask(taskName, projectName) {
  if (!taskName || !projectName) {
    handleError(name, 'empty');
    return;
  }

  const task = tasks.filter(task => task.name === taskName)[0];
  
  if (!task) {
    handleError(taskName, 'noTask');
    return;
  }
  
  let taskIsAssigned = false;

  projects.forEach(project => {
    let projectTasks = project.tasks

    projectTasks.forEach(projectTask => {
      if (projectTask.name === taskName)
      {
        taskIsAssigned = true;
      }
    });

  });

  if (taskIsAssigned) {
    handleError(taskName, 'assigned');
    return;
  }
  
  const project = projects.filter(project => project.name === projectName)[0];

  if (!project) {
    handleError(projectName, 'noProject');
    return;
  }

  const taskDays = task.estDays;

  task.inproject = projectName;
  project.tasks.push(task);
  project.deadline = adjustDeadline('add', project.deadline, taskDays);
  project.totalDays = (project.totalDays + taskDays);

  console.log(`Task assigned to ${project.name}! New deadline: ${project.deadline}`);
}


function adjustTaskDays(taskName, newEstDays) {
  if (!taskName || !newEstDays) {
    handleError(name, 'empty');
    return;
  }

  const task = tasks.filter(task => task.name === taskName)[0];

  if (!task) {
    handleError(taskName,'noTask');
    return;
  }

  const oldEstDays = task.estDays;
    

  if (newEstDays <= 0) {
    handleError(taskName,'estDays');
    return;
  } else if (newEstDays === oldEstDays) {
    return;
  }

  const daysDiff = parseInt(oldEstDays - newEstDays);
  let positiveDiff = daysDiff < 0 ? -daysDiff : daysDiff;

  task.estDays = newEstDays;
    
  if (task.inproject.length > 0) {
    const project = projects.filter(project => project.name === task.inproject)[0];

    if (newEstDays > oldEstDays) {
      project.deadline = adjustDeadline('add', project.deadline, positiveDiff);
      project.totalDays = (project.totalDays + positiveDiff);
    } else if (newEstDays < oldEstDays) {
      project.deadline = adjustDeadline('delete', project.deadline, positiveDiff);
      project.totalDays = (project.totalDays - positiveDiff);
    }

    console.log(`Estimated days adjusted to ${newEstDays}! New ${project.name} deadline: ${project.deadline}`);
  } else {
    console.log(`Estimated days adjusted to ${newEstDays}!`)
  }
}


function deleteTask(taskName) {
  if (!taskName) {
    handleError(name, 'empty');
    return;
  }

  const task = tasks.filter(task => task.name === taskName)[0];
  const index = tasks.indexOf(task);

  if (!task) {
    handleError(taskName, 'noTask')
    return;
  }

  if (index > -1) {
    tasks.splice(index, 1);
  }

  const projectName = task.inproject;
  
  if (projectName.length > 0) {
    const project = projects.filter(project => project.name === projectName)[0];
    const taskInProject = project.tasks.filter(task => task.name === taskName)[0];
    const taskInProjectIndex = project.tasks.indexOf(taskInProject);
  
    if (taskInProjectIndex > -1) {
      project.tasks.splice(taskInProjectIndex, 1);
    }
  
    project.deadline = adjustDeadline('delete', project.deadline, task.estDays);
    project.totalDays = (project.totalDays - task.estDays);

    console.log(`Task ${taskName} deleted and removed from ${project.name}! New deadline: ${project.deadline}`);
  } else {
    console.log(`Task ${taskName} deleted.`);
  }

  task.inproject = '';
}