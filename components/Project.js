let projects = []


function createProject(name, startDate, slackTime=0) {
  if (!name || !startDate) {
    handleError(name, 'empty');
    return;
  }

  if (slackTime < 0) {
    handleError(name, 'estDays');
    return;
  }

  const project = projects.filter(project => project.name === name)[0];

  if (project) {
    handleError(name, 'name');
    return;
  }

  if (!isValidDate(startDate)) {
    handleError(startDate, 'date');
    return;
  }

  projects.push({
    "name" : name,
    "startDate" : startDate,
    "slackTime" : parseInt(slackTime),
    "deadline" : adjustDeadline('add', startDate, parseInt(slackTime)),
    "totalDays" : parseInt(slackTime),
    "tasks" : [],
    "employee" : ""
  })

  const newProject = projects.filter(project => project.name === name)[0];
  console.log(`[Project] ${name} added! It will start at ${startDate} with${ slackTime > 0 ? ' ' + slackTime : 'out' } slack ${ slackTime > 1 ? 'days' : 'day' }. Estimated deadline: ${newProject.deadline}`);
}


function deleteProject(projectName) {
  if (!projectName) {
    handleError(name, 'empty');
    return;
  }

  const project = projects.filter(project => project.name === projectName)[0];
  const index = projects.indexOf(project);

  if (!project) {
    handleError(projectName, 'noProject');
    return;
  }

  if (index > -1) {
    projects.splice(index, 1);
  }

  const employeeName = project.employee;
  if (employeeName.length > 0) {
    const nameSplit = employeeName.split(' ');
    const employee = employees.filter(employee => employee.firstname === nameSplit[0] && employee.lastname === nameSplit[1])[0];
    
    let employeeProjects = employee.projects;
    const thisProject = employee.projects.filter(project => project === projectName)[0];
    
    const employeeProjectIndex = employeeProjects.indexOf(thisProject);

    if (employeeProjectIndex > -1) {
      employeeProjects.splice(employeeProjectIndex, 1);
    }
  }

  console.log(`Project ${projectName} deleted.`);
}
