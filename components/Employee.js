let employees = []


function createEmployee(firstname, lastname, supervisor='') {
  if (!firstname || !lastname) {
    handleError(name, 'empty');
    return;
  }

  const employee = employees.filter(employee => employee.firstname === firstname && employee.lastname === lastname)[0];

  if (employee) {
    handleError(`${firstname} ${lastname}`, 'name');
    return;
  }

  employees.push({
    "firstname" : firstname,
    "lastname" : lastname,
    "supervisor" : supervisor,
    "projects" : []
  })

  if (supervisor) {

    let splitSupervisor = supervisor.split(' ');
    const employee = employees.filter(employee => employee.firstname === splitSupervisor[0] && employee.lastname === splitSupervisor[1])[0];

    if (!employee) {
      createEmployee(splitSupervisor[0], splitSupervisor[1]);
    }
      
  }

  console.log(`[Employee] ${firstname} ${lastname} added! ${ supervisor ? 'Supervisor is ' + supervisor : '' }`);    
}


function assignEmployee(employeeName, projectName) {
  if (!employeeName || !projectName) {
    handleError(name, 'empty');
    return;
  }

  let splitEmployee = employeeName.split(' ');
  const employee = employees.filter(employee => employee.firstname === splitEmployee[0] && employee.lastname === splitEmployee[1])[0];

  if (!employee) {
    handleError(employeeName, 'noEmployee');
    return;
  }
  
  const project = projects.filter(project => project.name === projectName)[0];

  if (!project) {
    handleError(projectName, 'noProject');
    return;
  }

  const employeeHasProject = employee.projects.filter(project => project === projectName)[0];

  if(employeeHasProject)
  {
    handleError(employeeName, 'assigned');
    return;
  }

  if(employee.projects.length >= 2)
  {
    handleError(employeeName, 'projectsLimit');
    return;
  }

  if(project.employee.length > 0) {
    handleError(projectName, 'hasEmployee');
    return;
  }

  employee.projects.push(projectName);
  project.employee = employeeName;
  console.log(`${employeeName} assigned to ${project.name}!`);
}