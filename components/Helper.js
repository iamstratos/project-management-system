function view(type, projectName) {
  if (type === tasks) {

    if (projectName) {
      const project = projects.filter(project => project.name === projectName)[0];

      if (!project) {
        handleError(projectName, 'noProject');
        return;
      } else if (!project.tasks.length > 0) {
        handleError(projectName, 'noAssignTasks');
        return;
      }

      console.log(project.tasks.length)
      console.table(project.tasks);

    } else {
      console.log(`Please enter view(tasks, 'Project Name').`)
    }

  } else if (type === projects && projectName instanceof Array) {

    let total = 0;
    let projectsExists = true;

    projectName.forEach(arrName => {
      let project = projects.filter(project => project.name === arrName)[0];
      if (!project) {
        projectsExists = false;
        handleError(arrName, 'noProject')
        return;
      }

      let totalDays = projects.filter(project => project.name === arrName)[0].totalDays;
      if (totalDays) {
        total += totalDays;
      }
    });


    if (!projectsExists) {
      return;
    }
      
    console.log(`Your selected projects will need ${total} days to complete.`);
  } else {
    console.table(type);
  }
}


// Handle Errors in one place
function handleError(data, type) {
  switch(type) {
    case 'name':
      console.log(`[Error] ${data} exists. Please enter another ${type}.`);
      break;
    case 'date':
      console.log(`[Error] ${data} is not valid. Please enter another ${type}.`);
      break;
    case 'estDays':
      console.log(`[Error] You should enter at least 1 day.`);
      break;
    case 'assigned':
      console.log(`[Error] ${data} is already assigned.`);
      break;
    case 'noTask':
      console.log(`[Error] There is no task named ${data}.`);
      break;
    case 'noProject':
      console.log(`[Error] There is no project named ${data}.`);
      break;
    case 'noEmployee':
      console.log(`[Error] There is no employee named ${data}.`);
      break;
    case 'projectsLimit':
      console.log(`[Error] An employee can only work on two projects at the same time.`);
      break;
    case 'hasEmployee':
      console.log(`[Error] ${data} already has an employee.`);
      break;
    case 'empty':
      console.log(`[Error] Please fill all the arguments.`);
      break;
    case 'noAssignTasks':
      console.log(`[Error] ${data} has no assigned tasks.`);
      break;
  }
}


// Check for valid dates with the power of Moment.js
function isValidDate(string) {
  let date = moment(string,'D/M/YYYY');
  if(date == null || !date.isValid()) {
    return false;
  }

  return string.indexOf(date.format('D/M/YYYY')) >= 0;
}


function adjustDeadline(action, startDate, number) {
  if (action === 'add') {
    return moment(startDate, 'D/M/YYYY').add(number, 'days').format('D/M/YYYY');
  } else if (action === 'delete') {
    return moment(startDate, 'D/M/YYYY').subtract(number, 'days').format('D/M/YYYY');
  }
}