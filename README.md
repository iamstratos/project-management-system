# Project Management System - Docs

*A minimal Project Management System*

*How to:* Download and open **index.html**.

> **Note**: It's mostly written on ES6 so you may open it on your browser's latest version (IE not included).

## Create Project
```javascript
createProject(name, startDate, slackTime)
```
- `name` should be a string and it's required.
- `startDate` should be a string with format 'D/M/YYYY'. It's required.
- `slackTime` should be a number. It's 0 if left empty.
> e.g. `createProject('Project A', '22/10/2017', 3)`

> **Note**: Every Project name should be unique.

## Create Task
```javascript
createTask(name, description, estDays)
```
- `name` should be a string and it's required.
- `description` should be a string and it's required.
- `estDays` should be a number and at least 1. It's required.
> e.g. `createTask('Task A', 'This is task A!', 6)`

> **Note**: Every Task name should be unique.

## Create Employee
```javascript
createEmployee(firstname, lastname, supervisor)
```
- `firstname` should be a string and it's required.
- `lastname` should be a string and it's required.
- `supervisor` should be a string. Please include both first & last name here. If not found, he/she will be automatically created. If left empty, it will assign no supervisor.
> e.g. `createEmployee('Stratos', 'Iordanidis', 'Tom Holland')`

> **Note**: Every Employee firstname and lastname should be unique.


## Assign Task
```javascript
assignTask(taskName, projectName)
```
- `taskName` should be a string and it's required.
- `projectName` should be a string and it's required.
> e.g. `assignTask('Task A', 'Project A')`


## Assign Employee
```javascript
assignEmployee(employeeName, projectName)
```
- `employeeName` should be a string and it's required. Please include both first & last name here.
- `projectName` should be a string and it's required.
> e.g. `assignEmployee('Stratos Iordanidis', 'Project A')`

> **Note**: An Employee can only work on two projects at the same time.

## Adjust Task's estimated days
```javascript
adjustTaskDays(taskName, newEstDays)
```
- `taskName` should be a string and it's required.
- `newEstDays` should be a number and at least 1. It's required.
> e.g. `adjustTaskDays('Task A', 9)`


## Delete Task
```javascript
deleteTask(taskName)
```
- `taskName` should be a string and it's required.
> e.g. `deleteTask('Task A')`


## Delete Project
```javascript
deleteProject(projectName)
```
- `projectName` should be a string and it's required.
> e.g. `deleteProject('Project A')`


## View all employees
```javascript
view(employees)
```
- `employees` is the array of all employees. Can't be changed.
> No example here, use it as it is.


## View all tasks for a given project
```javascript
view(tasks, projectName)
```
- `tasks` is the array of all tasks. Can't be changed.
- `projectName` should be a string and it's required.
> e.g. `view(tasks, 'Project A')`


## View all projects
```javascript
view(projects)
```
- `projects` is the array of all projects. Can't be changed.
> No example here, use it as it is.


## Get total days needed for a given list of projects
```javascript
view(projects, [projectName])
```
- `projects` is the array of all projects. Can't be changed.
- `projectName` should be a string (or more than one, seperated with ,) and it's required.
> e.g. `view(projects, ['Project A', 'Project B', 'Project C'])`
